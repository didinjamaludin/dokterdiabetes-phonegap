cordova.define("com.affle.affletrackersdk.Affletracker", function(require, exports, module) {var affletracker = {

    /**
     * Main Function called by the Application to track views, events etc.
     * 
     * @param viewName   Current View name.
     * @param viewDetail A detail string describes for current View, "null" if it
     *                   weren't exist.
     * @param eventName  An event name on view, "null" if it weren't exist.
     */
    inAppTrackerViewName: function(viewName, viewDetail, eventName) {
        cordova.exec(
                    null, // success callback function
                    null, // error callback function
                    'AffleTracker', // mapped to our native Java class called "AffleTracker"
                    'inAppTrackerViewName', // with this action name
                    [{                  // and this array of custom arguments to create our entry
                        "viewName": viewName,
                        "viewDetail": viewDetail,
                        "eventName": eventName
                    }]
                );

    },

    /**
     * Main Function called by the Application to track for Purchase Event detail (view, pEvent, pTid, pAmount, pamount etc).
     *.
     * @param viewName   Current View name.
     * @param viewDetail A detail string describes for current View, "null" if it
     *                   weren't exist.
     * @param pevent     Purchase event name
     * @param ptid       Purchase event transaction id, returned by payment gateway or booking engine.
     * @param pqty       Purchase event quantity. Example: Number of tickets booked
     * @param pamount    Purchase Event Amount. Example:- 200, 5 etc
     */
    inAppEventTrackerViewName: function(viewName, viewDetail, pevent, ptid, pamount, pqty) {
        cordova.exec(
                    null, // success callback function
                    null, // error callback function
                    'AffleTracker', // mapped to our native Java class called "AffleTracker"
                    'inAppEventTrackerViewName', // with this action name
                    [{                  // and this array of custom arguments to create our entry
                        "viewName": viewName,
                        "viewDetail": viewDetail,
                        "pevent": pevent,
                        "ptid": ptid,
                        "pamount": pamount,
                        "pqty": pqty
                    }]
                );

    }
}});
