
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() { 
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CALORIEIDEAL', [], function(tx,results) {
                var clen = results.rows.length;
                if(clen>0) {
                    var gender = results.rows.item(0).gender;
                    var aktivitas = results.rows.item(0).aktivitas;
                    var berat = results.rows.item(0).berat;
                    var tinggi = results.rows.item(0).tinggi;
                    var bmi;
                    var la;
                    var ci;
                    if(gender=="wanita") {
                        bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*10)/100);
                        if(aktivitas=="sangat ringan") {
                            la="1.3";
                        } else if(aktivitas=="ringan") {
                            la="1.55";
                        } else if(aktivitas=="sedang") {
                            la="1.7";
                        } else {
                            la="2";
                        }
                        ci = (parseInt(bmi)*25)*parseInt(la);
                    } else if(gender=="pria") {
                        bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*15)/100);
                        if(aktivitas=="sangat ringan") {
                            la="1.3";
                        } else if(aktivitas=="ringan") {
                            la="1.65";
                        } else if(aktivitas=="sedang") {
                            la="1.76";
                        } else {
                            la="2.1";
                        }
                        ci = (parseInt(bmi)*30)*parseInt(la);
                    }
                    //$(".food-ideal-text").append("Kalori ideal Anda : <b>"+ci+"</b>");
                    //$("#calid").val(ci);
                    localStorage.setItem("calideal", parseInt(ci));
                } else {
                    $('#modal-foodFirst').show();
                }               
            });
        }, app.errorCB);
    },
    errorPop: function(tx, err) {
        alert("Error populating DB: "+err);
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    autocomplete: function(value,type) {
        if(value.length>0) {
            $('.'+type+'-autocomplete').show();
            app.autoFood(value,type);
        } else {
            $('.'+type+'-autocomplete').hide();
        }
    },
    validateCalIdeal: function() {
        var gender = $(".foodFirst-gender-value").val();
        var aktivitas = $("input[name='ff-activity']:checked").val();
        var berat = $("#berat").val();
        var tinggi = $("#tinggi").val();

        if(gender.length==0) {
            alert("Jenis Kelamin belum di Pilih");
            return;
        } else if(aktivitas.length==0) {
            alert("Aktivitas belum di Pilih");
            return;
        } else if(berat.length==0) {
            alert("Berat belum di isi");
            return;
        } else if(tinggi.length==0) {
            alert("Tinggi belum di isi");
            return;
        } else {
            if(pmodal==0) {
                var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
                db.transaction(function(tx) {
                    tx.executeSql('INSERT INTO CALORIEIDEAL(id, gender, aktivitas, berat, tinggi) VALUES(null,"'+gender+'","'+aktivitas+'","'+berat+'","'+tinggi+'")');
                }, app.errorCB, app.calsaveSuccess(gender,aktivitas,berat,tinggi));
            } else {
                var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
                db.transaction(function(tx) {
                    tx.executeSql('UPDATE CALORIEIDEAL SET gender="'+gender+'",aktivitas="'+aktivitas+'",berat="'+berat+'",tinggi="'+tinggi+'"');
                }, app.errorCB, app.calsaveSuccess(gender,aktivitas,berat,tinggi));
            }
        }
    },
    calsaveSuccess: function(gender,aktivitas,berat,tinggi) {
        var bmi;
        var la;
        var ci;
        if(gender=="wanita") {
            bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*10)/100);
            if(aktivitas=="sangat ringan") {
                la="1.3";
            } else if(aktivitas=="ringan") {
                la="1.55";
            } else if(aktivitas=="sedang") {
                la="1.7";
            } else {
                la="2";
            }
            ci = (parseInt(bmi)*25)*parseInt(la);
        } else if(gender=="pria") {
            bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*15)/100);
            if(aktivitas=="sangat ringan") {
                la="1.3";
            } else if(aktivitas=="ringan") {
                la="1.65";
            } else if(aktivitas=="sedang") {
                la="1.76";
            } else {
                la="2.1";
            }
            ci = (parseInt(bmi)*30)*parseInt(la);
        }
        $(".food-ideal-text").html("Kalori ideal Anda : <b>"+parseInt(ci)+"</b>");
        $("#calid").val(parseInt(ci));
    },
    getCalorieIdeal: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CALORIEIDEAL', [], function(tx,results) {
                var clen = results.rows.length;
                if(clen>0) {
                    var gender = results.rows.item(0).gender;
                    if(gender=='pria') {
                        $(".pria").click();
                    } else {
                        $(".wanita").click();
                    }
                    $(".foodFirst-gender-value").val(gender);
                    var akt = results.rows.item(0).aktivitas;
                    if(akt=='sangat ringan') {
                        $('input:radio[name=ff-activity]:nth(0)').screwDefaultButtons("check");
                    } else if(akt=='ringan') {
                        $('input:radio[name=ff-activity]:nth(1)').screwDefaultButtons("check");
                    } else if(akt=='sedang') {
                        $('input:radio[name=ff-activity]:nth(2)').screwDefaultButtons("check");
                    } else {
                        $('input:radio[name=ff-activity]:nth(3)').screwDefaultButtons("check");
                    }
                    $("#berat").val(results.rows.item(0).berat);
                    $("#tinggi").val(results.rows.item(0).tinggi);
                    pmodal = 1;
                }               
            });
        }, app.errorCB);
    }
};
