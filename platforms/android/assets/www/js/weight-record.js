var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() { 
        //app.getCalorieIdeal();     
        app.getWeightRecord();
        app.getDefaultHeight();
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    getDefaultHeight: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT tinggi FROM CALORIEIDEAL', [], function(tx,results) {
                var blen = results.rows.length;
                if(blen>0) {
                    $("#tinggi").val(results.rows.item(0).tinggi);                   
                }
            });
        }, app.errorCB);
    },
    getWeightRecord: function() {
        var weightdata = [];
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT weight,year,month,day FROM HISTORYRECORD ORDER BY id DESC LIMIT 60', [], function(tx,results) {
                var blen = results.rows.length;
                if(blen>0) {
                    for(var b=0;b<blen;b++) {
                        weightdata.push({
                            number: results.rows.item(b).weight,
                            year: results.rows.item(b).year,
                            month: results.rows.item(b).month,
                            day: results.rows.item(b).day
                        });
                    }
                    tx.executeSql('SELECT * FROM CALORIEIDEAL', [], function(tx,results2) {
                        var clen = results2.rows.length;
                        if(clen>0) {
                            var gender = results2.rows.item(0).gender;
                            var tinggi = results2.rows.item(0).tinggi;
                            if(gender=="pria") {
                                idealWeight = (parseInt(tinggi)-100) - (((parseInt(tinggi)-100)*10)/100);
                            }
                            if(gender=="wanita") {
                                idealWeight = (parseInt(tinggi)-100) - (((parseInt(tinggi)-100)*15)/100);
                            }                           
                            $("#gender").val(gender);
                            drawWeightChart(weightdata.reverse(),idealWeight);
                            $('.weight-chart-point').fastClick(function()
                            {
                                $('.weight-chart-point-bubble').hide();
                                $(this).find('.weight-chart-point-bubble').show();
                            });
                        }           
                    });
                    //var jsonWeight = JSON.stringify(weightdata);                    
                }
            });
        }, app.errorCB);
        var d = new Date();
        $("#day").val(d.getDate());
        $("#month").val(d.getMonth()+1);
        $("#year").val(d.getFullYear());
    },
    updateWeightRecord: function() {
        var berat = $("#berat").val();
        var tinggi = $("#tinggi").val();
        var year = $("#year").val();
        var month = $("#month").val();
        var day = $("#day").val();
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('UPDATE HISTORYRECORD SET weight="'+berat+'" WHERE year="'+year+'" AND month="'+month+'" AND day="'+day+'"');
        }, app.errorCB, app.saveSuccess);                
    },
    saveSuccess: function() {
        $("#berat").val("");
        $("#tinggi").val("");
        $("#year").val("-");
        $("#month").val("-");
        $("#day").val("-");
        app.getWeightRecord();
        window.location.reload();
    }
};