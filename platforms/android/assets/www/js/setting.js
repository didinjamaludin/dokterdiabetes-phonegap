var token;

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {        
        app.getDefaultProfile();
        token = device.uuid;

        $(".settings-form").ajaxForm({
            url: host+"api/updateprofile?token="+token, 
            beforeSend: function(){
                $('#modal-loader').show();
            },
            success: function(response)
            {
                var nama = response.nama;
                var hp = response.hp;
                var kota = response.kota;
                var photo = response.photo;
                var token = response.token;
                var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
                db.transaction(function(tx) {
                    tx.executeSql('UPDATE CREDENTIAL SET nama="'+nama+'",hp="'+hp+'",kota="'+kota+'",photo="'+photo+'",token="'+token+'",eventnot="true",chatnot="true",replynot="true",reminder="true"');
                },app.errorCB, app.userUpdated()); 
            },
            error: function()
            {
                $('#modal-loading-failed').show();       
            }
         
        });
    },
    getDefaultProfile: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CREDENTIAL', [], function(tx,results) {
                var len = results.rows.length;
                if (len>0) {
                    $("#photo-div").append('<img class="settings-block-profpic-img" src="http://articles.appdokter.com/'+results.rows.item(0).photo+'" alt=" " />');
                    $("input[name=photo]").val(results.rows.item(0).photo);
                    $("input[name=nama]").val(results.rows.item(0).nama);
                    $("input[name=hp]").val(results.rows.item(0).hp);
                    $("input[name=kota]").val(results.rows.item(0).kota);
                    if(results.rows.item(0).eventnot=="true") {
                        $("#eventnot").screwDefaultButtons("check");
                    } else {
                        $("#eventnot").screwDefaultButtons("uncheck");
                    }
                    if(results.rows.item(0).chatnot=="true") {
                        $("#chatnot").screwDefaultButtons("check");
                    } else {
                        $("#chatnot").screwDefaultButtons("uncheck");
                    }
                    if(results.rows.item(0).replynot=="true") {
                        $("#replynot").screwDefaultButtons("check");
                    } else {
                        $("#replynot").screwDefaultButtons("uncheck");
                    }
                    if(results.rows.item(0).reminder=="true") {
                        $("#reminder").screwDefaultButtons("check");
                    } else {
                        $("#reminder").screwDefaultButtons("uncheck");
                    }
                } else {
                    alert("belum ada profile");
                }
            });
        }, app.errorCB, app.checkUser);
    },
    errorPop: function(tx, err) {
        alert("Error populating DB: "+err);
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    userUpdated: function() {
        setTimeout(function(){
            location.reload();
        },2000);        
    }
};