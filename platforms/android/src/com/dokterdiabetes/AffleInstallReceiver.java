/**
 * Copyright (c) 2014 AD2C INDIA PVT. LTD
 * 
 *
 * @package       AD2CAMPAIGN
 * @copyright     AD2C INDIA PVT. LTD
 * @author        Alok Pabalkar <alok@ad2c.co>
 * @license       Proprietary
 * @Description   Affle's ad2campaign Platform Android SDK InstallReceiver File.
 *                
 */
 package com.affle.affletrackersdksample;


import com.affle.affledowloadtracker.AffleAppDownloadTracker;


/**
 *  @Method       AffleInstallReceiver
 *  @Description  This Class extends the SDK's (AffleAppDownloadTracker) class file
 */
public class AffleInstallReceiver extends AffleAppDownloadTracker {
}

