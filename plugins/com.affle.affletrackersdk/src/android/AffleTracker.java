package com.affle.affletrackersdk;

import com.affle.affleinapptracker.AffleInAppTracker;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TALE on 3/11/14.
 */
public class AffleTracker extends CordovaPlugin {

    public static final String ACTION_IN_APP_TRACKER_VIEW_NAME = "inAppTrackerViewName";
    public static final String ACTION_IN_EVENT_TRACKER_VIEW_NAME = "inAppEventTrackerViewName";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (args == null || args.length() == 0) {
            if (callbackContext != null) {
                callbackContext.error("Illegal arguments.");
            }
            return false;
        }

        final JSONObject arg = args.getJSONObject(0);
        if (arg == null) {
            if (callbackContext != null) {
                callbackContext.error("Illegal arguments.");
            }
            return false;
        }

        final String viewName = arg.getString("viewName");
        final String viewDetail = arg.getString("viewDetail");

        if (action.equals(ACTION_IN_APP_TRACKER_VIEW_NAME)) {
            final String eventName = arg.getString("eventName");
            AffleInAppTracker.inAppTrackerViewName(this.cordova.getActivity(), viewName, viewDetail, eventName);
            if (callbackContext != null) {
                callbackContext.success();
            }
            return true;
        } else if (action.equals(ACTION_IN_EVENT_TRACKER_VIEW_NAME)) {
            final String pevent= arg.getString("pevent");
            final String ptid= arg.getString("ptid");
            final String pamount= arg.getString("pamount");
            final int pqty= arg.getInt("pqty");
            AffleInAppTracker.inAppEventTrackerViewName(this.cordova.getActivity(), viewName, viewDetail, pevent, ptid, pamount, pqty);
            if (callbackContext != null) {
                callbackContext.success();
            }
            return true;
        }

        if (callbackContext != null) {
            callbackContext.error("Action not found.");
        }
        return false;
    }

}
