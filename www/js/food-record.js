
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() { 
        app.getTempFood();
        app.getCalIdeal();        
    },
    errorPop: function(tx, err) {
        alert("Error populating DB: "+err);
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    getTempFood: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM TEMPFOODRECORD', [], function(tx,results) {
                var tlen = results.rows.length;
                if(tlen>0) {
                    for(var t=0;t<tlen;t++) {
                        var thtm = '<li class="food-list-li clearfix"><div class="food-list-col1"><input type="number" class="food-list-number clickable" value="'+results.rows.item(t).qty+'" /><input type="hidden" value="'+results.rows.item(t).tcal+'" class="food-calorie" /></div><div class="food-list-col2"><div class="food-list-foodName">' + results.rows.item(t).foodname + '</div><div class="food-list-delete" onclick="$(this).parent().parent().remove()"></div></div></li>';
                        $('#'+results.rows.item(t).type+'-food-list').append(thtm);
                    }
                }
                var d = new Date();
                $("#food-day").val(d.getDate());
                $("#food-month").val(d.getMonth()+1);
                $("#food-year").val(d.getFullYear());               
            });
        }, app.errorCB);
    },
    getCalIdeal: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CALORIEIDEAL', [], function(tx,results) {
                var clen = results.rows.length;
                if(clen>0) {
                    var gender = results.rows.item(0).gender;
                    var aktivitas = results.rows.item(0).aktivitas;
                    var berat = results.rows.item(0).berat;
                    var tinggi = results.rows.item(0).tinggi;
                    var bmi;
                    var la;
                    var ci;
                    if(gender=="wanita") {
                        bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*10)/100);
                        if(aktivitas=="sangat ringan") {
                            la="1.3";
                        } else if(aktivitas=="ringan") {
                            la="1.55";
                        } else if(aktivitas=="sedang") {
                            la="1.7";
                        } else {
                            la="2";
                        }
                        ci = (parseInt(bmi)*25)*parseInt(la);
                    } else if(gender=="pria") {
                        bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*15)/100);
                        if(aktivitas=="sangat ringan") {
                            la="1.3";
                        } else if(aktivitas=="ringan") {
                            la="1.65";
                        } else if(aktivitas=="sedang") {
                            la="1.76";
                        } else {
                            la="2.1";
                        }
                        ci = (parseInt(bmi)*30)*parseInt(la);
                    }
                    $(".food-ideal-text").html("Kalori ideal Anda : <b>"+parseInt(ci)+"</b>");
                    $("#calid").val(parseInt(ci));
                    localStorage.setItem("calideal", parseInt(ci));
                }              
            });
        }, app.errorCB);
    },
    autocomplete: function(value,type) {
        if(value.length>0) {
            $('.'+type+'-autocomplete').show();
            app.autoFood(value,type);
        } else {
            $('.'+type+'-autocomplete').hide();
        }
    },
    autoFood: function(params,type) {
        var plist = '';
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CALORIE WHERE foodname LIKE "%'+params+'%" LIMIT 5', [], function(tx,results) {
                var clen = results.rows.length;
                if(clen>0) {
                    $('.'+type+'-autocomplete').empty();
                    for(var x=0;x<clen;x++) {
                        plist += '<li onclick="app.pickFood(\''+results.rows.item(x).foodname+'\',\''+results.rows.item(x).id+'\',\''+results.rows.item(x).weight+'\',\''+results.rows.item(x).calorie+'\',\''+type+'\')">'+results.rows.item(x).foodname+'</li>';
                    }
                    $('.'+type+'-autocomplete').append(plist);
                }               
            });
        }, app.errorCB);
    },
    pickFood: function(foodname, id, weight, calorie, type) {
        $('#'+type+'-food-list').append('<li class="food-list-li clearfix"><div class="food-list-col1"><input type="number" class="food-list-number clickable" value="1" /><input type="hidden" value="'+calorie+'" class="food-calorie" /></div><div class="food-list-col2"><div class="food-list-foodName">' + foodname + '</div><div class="food-list-delete" onclick="$(this).parent().parent().remove()"></div></div></li>');
        $('.'+type+'-autocomplete').empty();
        $('.'+type+'-autocomplete').hide();
        $("#"+type+"-food-field").val("");
    },
    validateCalIdeal: function() {
        var gender = $(".foodFirst-gender-value").val();
        var aktivitas = $("input[name='ff-activity']:checked").val();
        var berat = $("#berat").val();
        var tinggi = $("#tinggi").val();

        if(gender.length==0) {
            alert("Jenis Kelamin belum di Pilih");
            return;
        } else if(aktivitas.length==0) {
            alert("Aktivitas belum di Pilih");
            return;
        } else if(berat.length==0) {
            alert("Berat belum di isi");
            return;
        } else if(tinggi.length==0) {
            alert("Tinggi belum di isi");
            return;
        } else {
            if(pmodal==0) {
                var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
                db.transaction(function(tx) {
                    tx.executeSql('INSERT INTO CALORIEIDEAL(id, gender, aktivitas, berat, tinggi) VALUES(null,"'+gender+'","'+aktivitas+'","'+berat+'","'+tinggi+'")');
                }, app.errorCB, app.calsaveSuccess(gender,aktivitas,berat,tinggi));
            } else {
                var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
                db.transaction(function(tx) {
                    tx.executeSql('UPDATE CALORIEIDEAL SET gender="'+gender+'",aktivitas="'+aktivitas+'",berat="'+berat+'",tinggi="'+tinggi+'"');
                }, app.errorCB, app.calsaveSuccess(gender,aktivitas,berat,tinggi));
            }
        }
    },
    calsaveSuccess: function(gender,aktivitas,berat,tinggi) {
        var bmi;
        var la;
        var ci;
        if(gender=="wanita") {
            bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*10)/100);
            if(aktivitas=="sangat ringan") {
                la="1.3";
            } else if(aktivitas=="ringan") {
                la="1.55";
            } else if(aktivitas=="sedang") {
                la="1.7";
            } else {
                la="2";
            }
            ci = (parseInt(bmi)*25)*parseInt(la);
        } else if(gender=="pria") {
            bmi = (parseInt(tinggi)-100)-(((parseInt(tinggi)-100)*15)/100);
            if(aktivitas=="sangat ringan") {
                la="1.3";
            } else if(aktivitas=="ringan") {
                la="1.65";
            } else if(aktivitas=="sedang") {
                la="1.76";
            } else {
                la="2.1";
            }
            ci = (parseInt(bmi)*30)*parseInt(la);
        }
        $(".food-ideal-text").html("Kalori ideal Anda : <b>"+parseInt(ci)+"</b>");
        $("#calid").val(parseInt(ci));
        localStorage.setItem("calideal", parseInt(ci));
    },
    countCalorie: function() {
        var total = 0;

        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM TEMPFOODRECORD');
            var breakfast = [];
            $("#breakfast-food-list li").each(function() {
                var nbr = $(this).find(".food-list-number").val();
                var fname = $(this).find(".food-list-foodName").text();
                var cal = $(this).find(".food-calorie").val();
                total+=parseInt(nbr)*parseInt(cal);
                var btotal = parseInt(nbr)*parseInt(cal);
                var bcont = { breakfastname:fname,breakfastnumber:nbr,breakfastcal:btotal };
                breakfast.push(bcont);
                tx.executeSql('INSERT INTO TEMPFOODRECORD(id, type, foodname, qty, tcal) VALUES(null,"breakfast","'+fname+'","'+nbr+'","'+btotal+'")');
            });
            localStorage.setItem("breakfast",JSON.stringify(breakfast));

            var lunch = [];
            $("#lunch-food-list li").each(function() {
                var nbr = $(this).find(".food-list-number").val();
                var fname = $(this).find(".food-list-foodName").text();
                var cal = $(this).find(".food-calorie").val();
                total+=parseInt(nbr)*parseInt(cal);
                var ltotal = parseInt(nbr)*parseInt(cal);
                var lcont = { lunchname:fname,lunchnumber:nbr,lunchcal:ltotal };
                lunch.push(lcont);
                tx.executeSql('INSERT INTO TEMPFOODRECORD(id, type, foodname, qty, tcal) VALUES(null,"lunch","'+fname+'","'+nbr+'","'+ltotal+'")');
            });
            localStorage.setItem("lunch",JSON.stringify(lunch));

            var dinner = [];
            $("#dinner-food-list li").each(function() {
                var nbr = $(this).find(".food-list-number").val();
                var fname = $(this).find(".food-list-foodName").text();
                var cal = $(this).find(".food-calorie").val();
                total+=parseInt(nbr)*parseInt(cal);
                var dtotal = parseInt(nbr)*parseInt(cal);
                var dcont = { dinnername:fname,dinnernumber:nbr,dinnercal:dtotal };
                dinner.push(dcont);
                tx.executeSql('INSERT INTO TEMPFOODRECORD(id, type, foodname, qty, tcal) VALUES(null,"dinner","'+fname+'","'+nbr+'","'+dtotal+'")');
            });
            localStorage.setItem("dinner",JSON.stringify(dinner));

            var snack = [];
            $("#snack-food-list li").each(function() {
                var nbr = $(this).find(".food-list-number").val();
                var fname = $(this).find(".food-list-foodName").text();
                var cal = $(this).find(".food-calorie").val();
                total+=parseInt(nbr)*parseInt(cal);
                var stotal = parseInt(nbr)*parseInt(cal);
                var scont = { snackname:fname,snacknumber:nbr,snackcal:stotal };
                snack.push(scont);
                tx.executeSql('INSERT INTO TEMPFOODRECORD(id, type, foodname, qty, tcal) VALUES(null,"snack","'+fname+'","'+nbr+'","'+stotal+'")');
            });
            localStorage.setItem("snack",JSON.stringify(snack));

            localStorage.setItem("totalcal", total);
            localStorage.setItem("day", $("#food-day").val());
            localStorage.setItem("month",$("#food-month").val());
            localStorage.setItem("year",$("#food-year").val());

        }, app.errorCB);

        //localStorage.setItem("calideal", $("#calid").val());

        $('.layer-overlay2').addClass('active');
        
        setTimeout(function()
        {
            window.location.href = "record_food_result.html";
        }, 400);
    },
    getCalorieIdeal: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CALORIEIDEAL', [], function(tx,results) {
                var clen = results.rows.length;
                if(clen>0) {
                    var gender = results.rows.item(0).gender;
                    if(gender=='pria') {
                        $(".pria").click();
                    } else {
                        $(".wanita").click();
                    }
                    $(".foodFirst-gender-value").val(gender);
                    var akt = results.rows.item(0).aktivitas;
                    if(akt=='sangat ringan') {
                        $('input:radio[name=ff-activity]:nth(0)').screwDefaultButtons("check");
                    } else if(akt=='ringan') {
                        $('input:radio[name=ff-activity]:nth(1)').screwDefaultButtons("check");
                    } else if(akt=='sedang') {
                        $('input:radio[name=ff-activity]:nth(2)').screwDefaultButtons("check");
                    } else {
                        $('input:radio[name=ff-activity]:nth(3)').screwDefaultButtons("check");
                    }
                    $("#berat").val(results.rows.item(0).berat);
                    $("#tinggi").val(results.rows.item(0).tinggi);
                    pmodal = 1;
                }               
            });
        }, app.errorCB);
    }
};
