var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {      
        app.getGlucoseRecord();
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    getGlucoseRecord: function() {
        var glucodata = [];
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT glucose,year,month,day FROM HISTORYRECORD ORDER BY id DESC LIMIT 60', [], function(tx,results) {
                var glen = results.rows.length;
                if(glen>0) {
                    for(var g=0;g<glen;g++) {
                        glucodata.push({
                            number: parseInt(results.rows.item(g).glucose),
                            year: parseInt(results.rows.item(g).year),
                            month: parseInt(results.rows.item(g).month),
                            day: parseInt(results.rows.item(g).day) 
                        });
                    }
                    //var jsonGlucose = JSON.stringify(glucodata);     
                    app.displayChart(glucodata.reverse());              
                }
            });
        }, app.errorCB);
        var d = new Date();
        $("#day").val(d.getDate());
        $("#month").val(d.getMonth()+1);
        $("#year").val(d.getFullYear());
    },
    displayChart: function(glucose) {
        drawGlucoseChart(glucose);
        $('.weight-chart-point').fastClick(function()
        {
            $('.weight-chart-point-bubble').hide();
            $(this).find('.weight-chart-point-bubble').show();
        });
    },
    updateGlucoseRecord: function() {
        var gluco = $("#gluco-field").val();
        var year = $("#year").val();
        var month = $("#month").val();
        var day = $("#day").val();
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('UPDATE HISTORYRECORD SET glucose="'+gluco+'" WHERE year="'+year+'" AND month="'+month+'" AND day="'+day+'"');
        },app.errorCB, app.saveSuccess); 
    },
    saveSuccess: function() {
        $("#gluco-field").val("");
        $("#year").val("-");
        $("#month").val("-");
        $("#day").val("-");
        app.getGlucoseRecord();
        window.location.reload();
    }
};