var token;
var docid;

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);        
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() { 
        app.getToken();
    },
    errorPop: function(tx, err) {
        alert("Error populating DB: "+err);
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    getToken: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CREDENTIAL', [], function(tx, results) {
                if (results.rows.length>0) {
                    token = results.rows.item(0).token;  
                    $("#chat-file-form-2").ajaxForm({
                        url: host+"api/sendFile?token="+token+"&docid=2",
                        beforeSend: function(){
                            $('#modal-loader').show();
                        },
                        success: function()
                        {
                            $('#modal-loader').hide();
                        },
                        error: function()
                        {
                            $('#modal-loader').hide();
                            $('#modal-loading-failed').show();
                     
                        }
                    }); 
                    $("#chat-file-form-3").ajaxForm({
                        url: host+"api/sendFile?token="+token+"&docid=3",
                        beforeSend: function(){
                            $('#modal-loader').show();
                        },
                        success: function()
                        {
                            $('#modal-loader').hide();
                        },
                        error: function()
                        {
                            $('#modal-loader').hide();
                            $('#modal-loading-failed').show();
                     
                        }
                    }); 
                    $("#chat-file-form-4").ajaxForm({
                        url: host+"api/sendFile?token="+token+"&docid=4",
                        beforeSend: function(){
                            $('#modal-loader').show();
                        },
                        success: function()
                        {
                            $('#modal-loader').hide();
                        },
                        error: function()
                        {
                            $('#modal-loader').hide();
                            $('#modal-loading-failed').show();
                     
                        }
                    });                  
                } else {
                    alert("no user found");
                }
            });
        }, app.getDoctor());
    },
    chatSend: function(param,msgid) {
        //$('#modal-loader').show();
        var form = $("#"+param);
        var params = form.serialize();      
        htm='<div class="chat-content-post2 clearfix">';
        htm+='<img class="chat-content-post-img" src="asset/img/chat/profile.png" alt=" " />';
        htm+='<div class="chat-content-post-detail">';
        htm+='<div class="chat-content-post-text-cont">';
        htm+='<p class="chat-content-post-text">'+$("#"+msgid).val()+'</p>';
        htm+='</div>';
        htm+='<div class="chat-content-post-time-cont clearfix">';
        htm+='<img class="chat-content-post-time-icon" src="asset/img/chat/icon/clock.png" alt=" " style="display:none;" />';
        htm+='<time class="chat-content-post-time">sending..</time>';
        htm+='</div>';
        htm+='<div class="chat-content-post-detail-arrow1"></div>';
        htm+='<div class="chat-content-post-detail-arrow2"></div>';
        htm+='</div>';
        htm+='</div>';
        $("#chat-content-"+docid).append(htm);      

        $("#"+msgid).val("");

        $.ajax({
            type: "POST",
            url: host+"api/chatSend",
            data: params+"&token="+token,
            //processData: false,
            dataType: "json"
        }).done(function(data) {    
        }).fail(function( jqXHR, textStatus ) {
            alert(textStatus);
            return;
        });
    },
    sendFile: function(frmid) {
        $("#"+frmid).submit();
    },
    chatRetrieve: function(tk,doctor) {
        $.ajax({
            type: "GET",
            url: host+"api/receiveChat",
            data: "token="+tk+"&doctor="+doctor,
            dataType: "json"
        }).done(function(data) {
            $("#gizi-stat-"+doctor).text("Online");
            $("#chat-head-status-"+doctor).addClass("on"); 
            $("#chat-content-field-cont-"+doctor).show();   
            $("#chat-content-field-cont-"+doctor).addClass("active");
            $("#chat-content-"+doctor).empty();                    
            var htm="";
            htm+='<div class="chat-content-date-cont">';
            htm+='<span class="chat-content-date">'+new Date()+'</span>';
            htm+='</div>';  
            $.each(data, function(i,val) {              
                if(val.sender=='doctor') {              
                    htm='<div class="chat-content-post1 clearfix">';
                    htm+='<img class="chat-content-post-img" src="asset/img/chat/'+val.docphoto+'" alt=" " />';
                }
                if(val.sender=='user') {                    
                    htm='<div class="chat-content-post2 clearfix">';
                    htm+='<img class="chat-content-post-img" src="http://articles.appdokter.com/'+val.usrphoto+'" alt=" " />';
                }
                htm+='<div class="chat-content-post-detail">';
                htm+='<div class="chat-content-post-text-cont">';
                htm+='<p class="chat-content-post-text">'+val.message+'</p>';
                htm+='</div>';
                htm+='<div class="chat-content-post-time-cont clearfix">';
                htm+='<img class="chat-content-post-time-icon" src="asset/img/chat/icon/clock.png" alt=" " />';
                var d = new Date(val.sendDate);
                var n = d.getHours(val.sendDate);
                var m = d.getMinutes(val.sendDate);
                htm+='<time class="chat-content-post-time" id="post-time">'+n + ":" + m+'</time>';
                htm+='</div>';
                htm+='<div class="chat-content-post-detail-arrow1"></div>';
                htm+='<div class="chat-content-post-detail-arrow2"></div>';
                htm+='</div>';
                htm+='</div>';
                $("#chat-content-"+doctor).append(htm);
            });
            //$('#modal-loader').hide();
            blackBoxFix();
            $("#chatroom-"+doctor).parent().animate({ scrollTop: $("#chatroom-"+doctor).parent().height() }, "slow"); 
        }).fail(function(jqXHR, textStatus) {
            $('#modal-loading-failed').show();
            return;
        });                         
    },
    getDoctor: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM DOCTOR', [], function(tx,results) {
                var dlen = results.rows.length;
                if (dlen>0) {
                    for(var d=0;d<dlen;d++) {
                        var id = results.rows.item(d).id;
                        $("#chat-tab-text-"+id).empty();
                        if(id=='4') {
                            $("#chat-tab-text-"+id).append("Jantung");
                        } else if(id=='3') {
                            $("#chat-tab-text-"+id).append("Penyakit Dalam");
                        } else {
                            $("#chat-tab-text-"+id).append(results.rows.item(d).spesialis);
                        }                       
                        var htm = '<div class="chat-head clearfix">';
                        htm+='<img class="chat-head-img" src="asset/img/chat/'+results.rows.item(d).photo+'" alt=" " />';
                        htm+='<div class="chat-head-detail">';
                        htm+='<h3 class="chat-head-name">'+results.rows.item(d).nama+'</h3>';
                        htm+='<div class="chat-head-desc-cont">';
                        htm+='<p class="chat-head-desc2">'+results.rows.item(d).hospital+'</p>';
                        htm+='<p class="chat-head-desc">'+results.rows.item(d).spesialis+'</p>';
                        htm+='</div>';
                        htm+='<div class="chat-head-status" id="chat-head-status-'+id+'">Status : <label id="gizi-stat-'+id+'">Offline</label></div>';
                        htm+='</div>';
                        htm+='</div>';
                        $(htm).insertBefore($("#chat-content-"+id));
                        $("#doctor-id-"+id).val(id);                            
                    }                    
                }
            });
        }, app.getSchedule());     
    },
    getSchedule: function() {
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM CHATSCHEDULE', [], function(tx,results) {
                var slen = results.rows.length;
                if (slen>0) {
                    for(var s=0;s<slen;s++) {
                        var did = results.rows.item(s).doctor;
                        var sdhtm='<li class="chat-content-schedule-list-li clearfix">';
                        sdhtm+='<div class="chat-content-schedule-list-col1">'+results.rows.item(s).day+'</div>';
                        sdhtm+='<div class="chat-content-schedule-list-col2">'+results.rows.item(s).starttime+' - '+results.rows.item(s).endtime+' WIB</div>';
                        sdhtm+='</li>';
                        $("#schedlist-"+did).append(sdhtm);
                    }                    
                }
            });
        }, app.checkSchedule()); 
    },
    checkSchedule: function() {
        setTimeout(function(){
            var td = new Date();
            var thour = td.getHours();
            var tmin = td.getMinutes() < 10 ? '0' + td.getMinutes() : td.getMinutes();
            var ttime = thour+":"+tmin;
            var tday = getDayOfWeek(td.getDay());
            var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
            db.transaction(function(tx) {
                tx.executeSql('SELECT * FROM CHATSCHEDULE WHERE day="'+tday+'"', [], function(tx,results) {
                    var slen = results.rows.length;             
                    if (slen>0) {
                        for(var s=0;s<slen;s++) {
                            var stime = results.rows.item(s).starttime;
                            var etime = results.rows.item(s).endtime;
                            if(parseInt(ttime.replace(/[^a-zA-Z 0-9.]+/g,''))>parseInt(stime.replace(/[^a-zA-Z 0-9.]+/g,''))&&parseInt(ttime.replace(/[^a-zA-Z 0-9.]+/g,''))<parseInt(etime.replace(/[^a-zA-Z 0-9.]+/g,''))) {
                                docid = results.rows.item(s).doctor;
                                $("#gizi-stat-"+docid).text("Online");
                                $("#chat-head-status-"+docid).addClass("on");
                                $("#chat-content-"+docid).empty();
                                $("#chat-content-field-cont-"+docid).show();
                                $("#chat-content-field-cont-"+docid).addClass("active");                                
                            } 
                        }                    
                    }
                });
            });
        }, 2000);         
    }
};
