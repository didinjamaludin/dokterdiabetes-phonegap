var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {      
        app.getRecordHistory();
    },
    errorCB: function(tx, err) {
        alert("Error processing SQL: "+err);
    },
    getRecordHistory: function() {
        $(".history-list").empty();
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM HISTORYRECORD WHERE glucose<>0 OR weight<>0 OR calorie<>0 ORDER BY id DESC', [], function(tx,results) {                
                var blen = results.rows.length;
                if(blen>0) {
                    for(var b=0;b<blen;b++) {
                        var htm='<li class="history-list-li">';
                        htm+='<time class="history-date">'+results.rows.item(b).day+' '+results.rows.item(b).month+' '+results.rows.item(b).year+'</time>';
                        htm+='<div class="history-block">';
                        htm+='<ul class="history-block-list">';
                        htm+='<li class="history-block-list-li clearfix">';
                        htm+='<div class="history-block-list-col1">Glucose Record</div>';
                        htm+='<div class="history-block-list-col2">'+results.rows.item(b).glucose+' mg/dL</div>';
                        htm+='</li>';
                        htm+='<li class="history-block-list-li clearfix">';
                        htm+='<div class="history-block-list-col1">Weight Record</div>';
                        htm+='<div class="history-block-list-col2">'+results.rows.item(b).weight+' kg</div>';
                        htm+='</li>';
                        htm+='<li class="history-block-list-li clearfix">';
                        htm+='<div class="history-block-list-col1">Food Record</div>';
                        htm+='<div class="history-block-list-col2">'+results.rows.item(b).calorie+' cal</div>';
                        htm+='</li>';
                        htm+='</ul>';
                        htm+='</div>';
                        htm+='</li>';
                        $(".history-list").append(htm);
                    };
                }
            });
        }, app.errorCB);
    },
    exportRecordHistory: function() {
        //var line = 0;
        var doc = new jsPDF();

        var specialElementHandlers = {
            '#head-top': function(element, renderer){
                return true;
            }
        };

        doc.fromHTML($('body').get(0), 15, 15, {
            'width': 170, 
            'elementHandlers': specialElementHandlers
        });
         
        /*doc.setFontType("bold");
        doc.setFontSize(22);
        doc.text(20, 30, 'History Record');
        doc.setFontType("normal");
        doc.setFontSize(16);
        line = line+30;

        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM HISTORYRECORD WHERE glucose<>0 OR weight<>0 OR calorie<>0 ORDER BY id DESC', [], function(tx,results) {
                var blen = results.rows.length;
                if(blen>0) {
                    for(var b=0;b<blen;b++) {                        
                        var newline = (b*10)+line;
                        doc.text(20, newline, "Glucose Record");
                        doc.text(40, newline, results.rows.item(b).glucose);
                    }
                }
            });
        }, app.errorCB);*/
         
        var pdfOutput = doc.output();
         
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
         
           fileSystem.root.getFile("Download/history.pdf", {create: true}, function(entry) {
              var fileEntry = entry;
         
              entry.createWriter(function(writer) {
                 writer.onwrite = function(evt) {
                 alert("History exported to Download/history.pdf");
              };
         
                 writer.write( pdfOutput );
              }, function(error) {
                 alert(error);
              });
         
           }, function(error){
              alert(error);
           });
        },
        function(event){
         alert( evt.target.error.code );
        });
    },
    findByDate: function() {
        var day = $("#select-day").val();
        var month = $("#select-month").val();
        var year = $("#select-year").val();
        $(".history-list").empty();
        var db = window.openDatabase("DokterDiabetes", "1.0", "Dokter Diabetes DB", 2000000);
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM HISTORYRECORD WHERE (glucose<>0 OR weight<>0 OR calorie<>0) AND day="'+day+'" AND month="'+month+'" AND year="'+year+'" ORDER BY id DESC', [], function(tx,results) {
                var blen = results.rows.length;
                if(blen>0) {
                    for(var b=0;b<blen;b++) {
                        var htm='<li class="history-list-li">';
                        htm+='<time class="history-date">'+results.rows.item(b).day+' '+results.rows.item(b).month+' '+results.rows.item(b).year+'</time>';
                        htm+='<div class="history-block">';
                        htm+='<ul class="history-block-list">';
                        htm+='<li class="history-block-list-li clearfix">';
                        htm+='<div class="history-block-list-col1">Glucose Record</div>';
                        htm+='<div class="history-block-list-col2">'+results.rows.item(b).glucose+' mg/dL</div>';
                        htm+='</li>';
                        htm+='<li class="history-block-list-li clearfix">';
                        htm+='<div class="history-block-list-col1">Weight Record</div>';
                        htm+='<div class="history-block-list-col2">'+results.rows.item(b).weight+' kg</div>';
                        htm+='</li>';
                        htm+='<li class="history-block-list-li clearfix">';
                        htm+='<div class="history-block-list-col1">Food Record</div>';
                        htm+='<div class="history-block-list-col2">'+results.rows.item(b).calorie+' cal</div>';
                        htm+='</li>';
                        htm+='</ul>';
                        htm+='</div>';
                        htm+='</li>';
                        $(".history-list").append(htm);
                    };
                }
            });
        }, app.errorCB);
    }
};