function dppCountScore()
{
	var dppScore = 0;
	
	$('#dpp-submit').fastClick(function()
	{
		dppScore = 0;
		
		// QUESTION 1
		if($('#dpp-question1').find('input.checkbox1:checked').length)
		{
			dppScore += 2;
		}
		
		
		// QUESTION 2
		if($('#dpp-question2-radio1:checked').length)
		{
			dppScore += 2;
		}
		else if($('#dpp-question2-radio2:checked').length)
		{
			dppScore += 1;
		}
		
		
		// QUESTION 3
		if($('#dpp-question3-radio1:checked').length)
		{
			dppScore += 2;
		}
		else if($('#dpp-question3-radio2:checked').length)
		{
			dppScore += 1;
		}
		
		
		// QUESTION 6
		if($('#dpp-question6-radio2:checked').length)
		{
			dppScore += 1;
		}
		else if($('#dpp-question6-radio3:checked').length)
		{
			dppScore += 2;
		}
		else if($('#dpp-question6-radio4:checked').length)
		{
			dppScore += 3;
		}
		
		
		// QUESTION 7
		if($('#dpp-question7-radio2:checked').length)
		{
			dppScore += 1;
		}
		else if($('#dpp-question7-radio3:checked').length)
		{
			dppScore += 2;
		}
		else if($('#dpp-question7-radio4:checked').length)
		{
			dppScore += 3;
		}
		
		
		// QUESTION 8
		if($('#dpp-question8-radio2:checked').length)
		{
			dppScore += 1;
		}
		else if($('#dpp-question8-radio3:checked').length)
		{
			dppScore += 2;
		}
		else if($('#dpp-question8-radio4:checked').length)
		{
			dppScore += 3;
		}
		
		
		// QUESTION 9
		if($('#dpp-question9-radio2:checked').length)
		{
			dppScore += 1;
		}
		else if($('#dpp-question9-radio3:checked').length)
		{
			dppScore += 2;
		}
		else if($('#dpp-question9-radio4:checked').length)
		{
			dppScore += 3;
		}
		
		
		// QUESTION 10
		if($('#dpp-question10-radio2:checked').length)
		{
			dppScore += 1;
		}
		else if($('#dpp-question10-radio3:checked').length)
		{
			dppScore += 2;
		}
		else if($('#dpp-question10-radio4:checked').length)
		{
			dppScore += 3;
		}
		
		
		// QUESTION 14
		if($('#dpp-question14-radio2:checked').length)
		{
			dppScore += 1;
		}
		else if($('#dpp-question14-radio3:checked').length)
		{
			dppScore += 2;
		}
		
		
		// BMI
		var w = $('#dpp-berat').val();
		var h = $('#dpp-tinggi').val() / 100;
		var bmi = w / (h*h);
		
		if(bmi >= 30)
		{
			dppScore += 2;
		}
		else if(bmi < 30 && bmi >= 25)
		{
			dppScore += 1;
		}
		
		
		// UMUR
		var age = $('#dpp-umur').val();
		
		if(age > 40)
		{
			dppScore += 1;
		}
		
		
		
		$('.layer-overlay2').addClass('active');
		
		setTimeout(function()
		{
			window.location.href = 'dpp_result.html#'+dppScore;
		}, 400);
	});
}


// MAIN
$(document).ready(function()
{
	dppCountScore();
	
	if($('.dpp-result-number').length)
	{
		var resultScore = window.location.hash.substr(1);
		var riskLevel;
		
		$('.dpp-result-number').html(resultScore);
		
		if(resultScore < 9)
		{
			riskLevel = 'low';
		}
		else if(resultScore >= 9 && resultScore <= 14)
		{
			riskLevel = 'moderate';
		}
		if(resultScore > 14)
		{
			riskLevel = 'high';
		}
		
		$('#dpp-riskLevel').html(riskLevel);
	}
	
});