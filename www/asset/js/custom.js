var ia = 0; // is Animating
var sflag = 0;
//var host = "http://10.0.0.128:8080/dokterdiabetes/";
var host = "http://ec2-54-254-240-146.ap-southeast-1.compute.amazonaws.com/";
var currentSlider = 1;
var pmodal = 0;
//var idealWeight = 70;

function createLink(link, layer)
{
	$(link).fastClick(function()
	{
		
		$('.layer-overlay2').addClass('active');
		
		setTimeout(function()
		{
			window.location.href = layer;
		}, 400);
		
	});
}

function blackBoxFix()
{
    setTimeout(function()
    {
        $('.panel-main').addClass('blackBoxFix');
       
        setTimeout(function()
        {
            $('.panel-main').removeClass('blackBoxFix');
        }, 10);
    }, 300);
}


function loginCheck(flag, modal, layer)
{
	if(!$(modal).hasClass('active') && ia == 0 && flag == 1)
	{
		$(modal).show();
	}
	else if(!$(modal).hasClass('active') && ia == 0 && flag == 0)
	{
		$('.layer-overlay2').addClass('active');
		
		setTimeout(function()
		{
			window.location.href = layer;
		}, 400);
	}
}


function closeModal()
{
	$('.modal-close').fastClick(function()
	{
		$('.modal').hide();
	});
}

function closeFailedModal()
{
	$('.modal-failed-close').fastClick(function()
	{
		$('#modal-loading-failed').hide();
	});
}


function toggleMenu(panelMain)
{
	if(!panelMain.hasClass('active'))
	{
		panelMain.addClass('active');
	}
	else
	{
		panelMain.removeClass('active');
	}
}


function mofShowResult(result)
{
	$('.mof-guide').hide();
	$('.mof-answer-cont').show();
	$('.mof-answer-result-text.' + result).show();
	$('.mof-share-cont').show();
	
	$('.mof-button').off();
}


function foodListDelete()
{
	$('.food-list-delete').fastClick(function()
	{
		var li = $(this).closest('.food-list-li');
		
		li.remove();
	});
}



function foodFirstChooseGender()
{
	$('.foodFirst-gender-value').val('');
	
	$('.foodFirst-gender').fastClick(function()
	{
		$('.foodFirst-gender').removeClass('active');
		$(this).addClass('active');
		
		if($(this).hasClass('pria'))
		{
			$('.foodFirst-gender-value').val('pria');
		}
		else if($(this).hasClass('wanita'))
		{
			$('.foodFirst-gender-value').val('wanita');
		}
	});
}



// CHART FUNCTIONS //
function getDayOfWeek(daynbr) {
	var dayname;

	if(daynbr == 0) { dayname = 'Minggu' }
	if(daynbr == 1) { dayname = 'Senin' }
	if(daynbr == 2) { dayname = 'Selasa' }
	if(daynbr == 3) { dayname = 'Rabu' }
	if(daynbr == 4) { dayname = 'Kamis' }
	if(daynbr == 5) { dayname = 'Jum\'at' }
	if(daynbr == 6) { dayname = 'Sabtu' }
	
	return dayname;
}

function getMonthName(month)
{
	var newName;
	
	if(month == 1) { newName = 'January' }
	if(month == 2) { newName = 'February' }
	if(month == 3) { newName = 'March' }
	if(month == 4) { newName = 'April' }
	if(month == 5) { newName = 'May' }
	if(month == 6) { newName = 'June' }
	if(month == 7) { newName = 'July' }
	if(month == 8) { newName = 'August' }
	if(month == 9) { newName = 'September' }
	if(month == 10) { newName = 'October' }
	if(month == 11) { newName = 'November' }
	if(month == 12) { newName = 'December' }
	
	return newName;
}

function simplifyDayName(string)
{
	var newName;
	
	if(string == 'Sunday') { newName = 'Sun' }
	if(string == 'Monday') { newName = 'Mon' }
	if(string == 'Tuesday') { newName = 'Tue' }
	if(string == 'Wednesday') { newName = 'Wed' }
	if(string == 'Thursday') { newName = 'Thu' }
	if(string == 'Friday') { newName = 'Fri' }
	if(string == 'Saturday') { newName = 'Sat' }
	
	return newName;
}

function drawChartDay(number)
{
	var i;
	
	for(i=0; i<number; i++)
	{
		var j = (number-1) - i;		// Reversed Indexing (untuk hitung hari)
		var day = (j).days().ago();
		var dayName = simplifyDayName(day.getDayName());
	
		$('.weight-chart-list-li:eq('+i+')').find('.weight-chart-list-text').html(dayName);
	}
}

function drawChartPoint(number, data)
{
	var ideal = idealWeight;
	var top = ideal + 50;
	var bottom = ideal - 50;
	var low = ideal - 10;
	var high = ideal + 10;
	
	var i;
	
	for(i=0; i<number; i++)
	{
		var day = data[i].day;
		var month = getMonthName(data[i].month);
		var w = data[i].number;
		var x = i * 41;
		var y = (w - bottom);
		var visibility;
		var status = '';
		var flip = '';
		
		if(w > top) { y = 100 }
		if(w < bottom) { y = 0 }
		
		if(y > 75) { flip = 'flip' }
		
		if(w > 0) { visibility = 'block' } else { visibility = 'none' }
		if(w > high) { status = 'high' } else if(w < low) { status = 'low' }
		
		var content = 	'<div class="weight-chart-point clickable '+status+' '+flip+'" style="left:'+x+'px; bottom: '+y+'%; display: '+visibility+'; ">	<div class="weight-chart-point-bubble">	<span class="weight-chart-point-bubble-text1">'+month+' '+day+'</span>	<span class="weight-chart-point-bubble-text2">'+w+' kg</span>	<span class="weight-chart-point-bubble-arrow"></span> </div> </div>'
		
		$('.weight-chart-point-cont').append(content);
	}
}

function drawChartPoint2(number, data)
{
	var top = 235;
	var bottom = 20;
	var low = 70;
	var high = 140;
	
	var i;

	for(i=0; i<number; i++)
	{
		var day = data[i].day;
		var month = getMonthName(data[i].month);
		var w = data[i].number;
		var x = i * 41;
		var y = (w - bottom);
		var visibility;
		var status = '';
		var flip = '';
		
		if(w > top) { y = 215 }
		if(w < bottom) { y = 0 }
		
		if(y > 162) { flip = 'flip' }
		
		if(w > 0) { visibility = 'block' } else { visibility = 'none' }
		if(w > high) { status = 'high' } else if(w < low) { status = 'low' }
		
		var content = 	'<div class="weight-chart-point clickable '+status+' '+flip+'" style="left:'+x+'px; bottom: '+y+'px; display: '+visibility+'; ">	<div class="weight-chart-point-bubble">	<span class="weight-chart-point-bubble-text1">'+month+' '+day+'</span>	<span class="weight-chart-point-bubble-text2">'+w+' mg/dL</span>	<span class="weight-chart-point-bubble-arrow"></span> </div> </div>'
		
		$('.weight-chart-point-cont').append(content);
	}
}

function drawChartLine(number, data)
{
	var ideal = idealWeight;
	var top = ideal + 50;
	var bottom = ideal - 50;
	var low = ideal - 10;
	var high = ideal + 10;
	var totalCoord = 0;
	var coord = new Array();
	
	for(i=0; i<number; i++)
	{
		var w = data[i].number;
		
		if(w > 0)
		{
			var x = i * 41;
			var y = (100 - (w - bottom)) * 215 / 100;
			
			if(w > top) { y = 0 }
			if(w < bottom) { y = 100 }
			
			coord.push(
			{
				'x' : x,
				'y' : y
			});
			
			totalCoord++;
		}
	}
	
	for(i=0; i<(totalCoord-1); i++)
	{
		var x1 = coord[i].x;
		var x2 = coord[i+1].x;
		var y1 = coord[i].y;
		var y2 = coord[i+1].y;
		
		var content = '<line x1="'+x1+'" y1="'+y1+'%" x2="'+x2+'" y2="'+y2+'%" style="stroke:#c3262e;stroke-width:3" />';
		
		//$('.weight-chart-svg').addClass('test');
		
		var obj = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		obj.setAttributeNS(null, 'x1', x1);
		obj.setAttributeNS(null, 'y1', y1);
		obj.setAttributeNS(null, 'x2', x2);
		obj.setAttributeNS(null, 'y2', y2);
		obj.setAttributeNS(null, 'stroke', '#c3262e');
		obj.setAttributeNS(null, 'stroke-width', 3);
		$('.weight-chart-svg')[0].appendChild(obj);
	}
}

function drawChartLine2(number, data)
{
	var top = 235;
	var bottom = 20;
	var low = 70;
	var high = 140;
	var totalCoord = 0;
	var coord = new Array();
	
	for(i=0; i<number; i++)
	{
		var w = data[i].number;
		
		if(w > 0)
		{
			var x = i * 41;
			var y = 235 - w;
			
			if(w > top) { y = 0 }
			if(w < bottom) { y = 215 }
			
			coord.push(
			{
				'x' : x,
				'y' : y
			});
			
			totalCoord++;
		}
	}
	
	for(i=0; i<(totalCoord-1); i++)
	{
		var x1 = coord[i].x;
		var x2 = coord[i+1].x;
		var y1 = coord[i].y;
		var y2 = coord[i+1].y;
		
		var content = '<line x1="'+x1+'" y1="'+y1+'%" x2="'+x2+'" y2="'+y2+'%" style="stroke:#c3262e;stroke-width:3" />';
		
		//$('.weight-chart-svg').addClass('test');
		
		var obj = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		obj.setAttributeNS(null, 'x1', x1);
		obj.setAttributeNS(null, 'y1', y1);
		obj.setAttributeNS(null, 'x2', x2);
		obj.setAttributeNS(null, 'y2', y2);
		obj.setAttributeNS(null, 'stroke', '#c3262e');
		obj.setAttributeNS(null, 'stroke-width', 3);
		$('.weight-chart-svg')[0].appendChild(obj);
	}
}

function drawWeightChart(weight,iw)
{
	drawChartDay(60);
	drawChartPoint(60, weight);
	drawChartLine(60, weight);
	
	$('.weight-chart-ideal-text2').html(iw + ' kg');
}

function drawGlucoseChart(glucose)
{
	drawChartDay(60);
	drawChartPoint2(60, glucose);
	drawChartLine2(60, glucose);
}
// CHART FUNCTIONS END //

function createChatSlider() {
	var chatSlider = $('.chat-slider').bxSlider(
	{
		'infiniteLoop' : false,
		'pager' : false,
		'controls' : false,
		'oneToOneTouch' : false,
		'swipeThreshold' : 50,
		'preventDefaultSwipeX' : false,
		onSlideBefore: function() 
		{
			currentSlider = chatSlider.getCurrentSlide() + 1;
			
			$('.chat-tab-list-li').removeClass('active');
			$('.chat-tab-list-li.li' + currentSlider).addClass('active');
		},
	});
	
	$('.chat-tab-list-li').fastClick(function()
	{
		var index = $(this).index();
		chatSlider.goToSlide(index);
	});

	return chatSlider;
}

// DOCUMENT READY //
$(document).ready(function()
{
	// CUSTOM BUTTONS / FIELD //
	$('.radio1, .checkbox1').screwDefaultButtons({ 
		image: 'url(asset/img/radio1.png)',
		width:	 23,
		height:	 24
	});
	
	$('.settings-block-list-cb').screwDefaultButtons({ 
		image: 'url(asset/img/settings/cb.png)',
		width:	 62,
		height:	 23
	});
	// CUSTOM BUTTONS / FIELD END //


	// OVERLAY //
	setTimeout(function()
	{
		$('.layer-overlay1').removeClass('active');
	}, 800);
	
	setTimeout(function()
	{
		$('.layer-overlay1').hide();
	}, 1200);
	// OVERLAY END //

	
	// FAST CLICK //
	$('.clickable').fastClick(function() {});
	// FAST CLICK END //
	
	
	// JSCROLL //
	// JSCROLL END //
	
	
	// LINK //
	createLink('.link-home', 'index.html');
	createLink('.link-consult', 'consult.html');
	createLink('.link-chat', 'chat.html');
	createLink('.link-message', 'message.html');
	createLink('.link-message2', 'message.html?tab=2');
	createLink('.link-message3', 'message.html?tab=3');
	createLink('.link-message4', 'message.html?tab=4');
	createLink('.link-article', 'article.html');
	createLink('.link-articleDetail', 'article_detail.html');
	createLink('.link-articleDetail-dummy1', 'article_detail_dummy1.html');
	createLink('.link-articleDetail-dummy2', 'article_detail_dummy2.html');
	createLink('.link-articleDetail-dummy3', 'article_detail_dummy3.html');
	createLink('.link-articleDetail-dummy4', 'article_detail_dummy4.html');
	createLink('.link-articleDetail-dummy5', 'article_detail_dummy5.html');
	createLink('.link-record', 'record.html');
	createLink('.link-record-glucose', 'record_glucose.html');
	createLink('.link-record-weight', 'record_weight.html');
	createLink('.link-record-food', 'record_food.html');
	createLink('.link-record-history', 'record_history.html');
	createLink('.link-games', 'games.html');
	createLink('.link-mof', 'mof.html');
	createLink('.link-dpp', 'dpp.html');
	createLink('.link-toko', 'toko.html');
	createLink('.link-settings', 'settings.html');
	createLink('.link-toc', 'toc.html');
	createLink('.link-contact', 'contact.html');
	
		
	$('.modalLink-signup').fastClick(function()
	{
		loginCheck(sflag, '#modal-signup', 'consult.html');
	});
	
	closeModal();
	closeFailedModal();
	// LINK END //
	
	
	// MENU //
	$('.menu-icon, .panel-overlay').fastClick(function()
	{
		var panelMain = $(this).closest('.panel-main');
		
		toggleMenu(panelMain);
	});
	
	$('.menu-list-link').fastClick(function()
	{
		var panelMain = $(this).closest('.panel-cont').find('.panel-main');
			
		setTimeout(function()
		{
			toggleMenu(panelMain);
		}, 100);
	});
	// MENU END //
	
	
	// CHAT //

	createChatSlider();
	
	// CHAT END //
	
	
	// HISTORY //
	$('.history-filter-button').fastClick(function()
	{
		$('.history-filter-button').removeClass('active');
		$(this).addClass('active');
	});
	
	$('.history-filter-button.all').fastClick(function()
	{
		$('.history-filter-date-cont').hide();
		$('.history-list').css('paddingTop', '');
		app.getRecordHistory();
	});
	
	$('.history-filter-button.date').fastClick(function()
	{
		$('.history-filter-date-cont').show();
		$('.history-list').css('paddingTop', '123px');
	});

	$('.history-save').fastClick(function()
	{
		app.exportRecordHistory();
	});
	// HISTORY END //
	
	
	// MOF //
	$('.mof-button.mitos').fastClick(function()
	{
		if($(this).hasClass('rightAnswer'))
		{
			mofShowResult('text1');
		}
		else
		{
			mofShowResult('text2');
		}
		
		$(this).addClass('active');
	});
	
	$('.mof-button.fakta').fastClick(function()
	{
		if($(this).hasClass('rightAnswer'))
		{
			mofShowResult('text3');
		}
		else
		{
			mofShowResult('text4');
		}
		
		$(this).addClass('active');
	});
	// MOF END //
	
	
	// FOOD //
	$('.food-field-button').fastClick(function()
	{
		var field = $(this).parent().children('.food-field');
		var list = $(this).closest('.food-block').find('.food-list');
		var value = field.val();
		var li = '<li class="food-list-li clearfix"><div class="food-list-col1"><input type="number" class="food-list-number clickable" value="1" /><div class="food-list-number-arrow"></div></div><div class="food-list-col2"><div class="food-list-foodName">' + value + '</div><div class="food-list-delete clickable"></div></div></li>'
		
		list.append(li);
		
		field.val('');
		
		foodListDelete();
	});
	
	foodListDelete();
	
	$('.food-ideal-edit, .record-profile-edit').fastClick(function()
	{
		app.getCalorieIdeal();
		$('#modal-foodFirst').show();
	});
	
	foodFirstChooseGender();
	// FOOD END //
	
	
	// WEIGHT //
	$('.weight-chart-viewport').mCustomScrollbar(
	{
		axis : 'x',
		scrollInertia: 0,
		keyboard:{ scrollAmount: 1 },
		mouseWheel:{ scrollAmount: 1 },
		contentTouchScroll: 15
	});
	
	$('.weight-chart-viewport').mCustomScrollbar('scrollTo', 'right');
	
	/*if($('#weight-chart').length)
	{
		drawWeightChart();
	}
	
	$('.weight-chart-point').fastClick(function()
	{
		$('.weight-chart-point-bubble').hide();
		$(this).find('.weight-chart-point-bubble').show();
	});*/
	// WEIGHT END //
	
	
	// GLUCOSE //
	/*if($('#glucose-chart').length)
	{
		drawGlucoseChart(glucose);
		
		$('.weight-chart-point').fastClick(function()
		{
			$('.weight-chart-point-bubble').hide();
			$(this).find('.weight-chart-point-bubble').show();
		});
	}*/
	// GLUCOSE END //
});


// PHONEGAP //
document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
		
	document.addEventListener("backbutton", function(e)
	{
		if(document.URL != 'index.html')
		{
			$('.layer-overlay2').addClass('active');
			setTimeout(function()
			{
				window.history.go(-1);
				
				setTimeout(function()
				{
					navigator.app.exitApp();
				}, 1500);
			}, 400);
		}
	}, false);
}
